package HostTable

import (
	"sync"
)

var table map[string]string //key是hostname，value是IP
var tableMutex sync.RWMutex

//更新HostTable或向其中添加一条记录
//
//输入一个要解析的域名和一个地址，这个地址可以是域名或IP
//
//返回错误信息和更新标记，如果在表中本来就有了一个一样的域名记录，那更新标记就为false
func update(hostname, addr string) (error, bool) {
	tableMutex.Lock()
	defer tableMutex.Unlock()
	newip, err := getIP(addr)
	if err != nil {
		return err, false
	}
	ipstr := newip.String()                            //获取更新的IP地址
	if ip, ok := table[hostname]; !ok || ip != ipstr { //如果这个Host不存在或和新值不一样
		table[hostname] = ipstr //那就更新
		return nil, true        //并且设更新标记为true
	}
	return nil, false //否则false
}

//删除HostTable中的某个Host
//
//如果要删除的Host原本就不存在，则返回false，否则返回true
func del(hostname string) bool {
	tableMutex.Lock()
	defer tableMutex.Unlock()
	if _, ok := table[hostname]; ok {
		delete(table, hostname)
		return true
	}
	return false
}

func Update(hostname, addr string) error {
	err := Delete(hostname) //必须要先删除这个hostname在本地的解析，getIP才能正常工作
	if err != nil {
		return err
	}
	err, ok := update(hostname, addr) //更新HostTable
	if err != nil {
		return err
	}
	if ok { //如果有更新
		err := ToFile(path) //那就写入文件
		if err != nil {
			return err
		}
	}
	return nil
}

func Delete(hostname string) error {
	ok := del(hostname)
	if ok { //如果有更新
		err := ToFile(path) //那就写入文件
		if err != nil {
			return err
		}
	}
	return nil
}
