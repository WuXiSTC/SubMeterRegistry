package Table

//更新表中的键值对，如果表中的键值对本来就是输入的这样就返回false
func (tb *Table) update(k, v string) bool {
	if oldv, ok := tb.t[k]; !ok || oldv != v { //如果表中不存在此键值对或者值不一样
		tb.t[k] = v //就更新
		return true //且返回true
	}
	return false //否则返回false
}

/**
两个线程安全的更新方法
*/

func (tb *Table) Update(k, v string) bool {
	tb.mu.Lock()
	defer tb.mu.Unlock()
	return tb.update(k, v)
}

func (tb *Table) UpdateAll(mp map[string]string) bool {
	updated := false
	tb.mu.Lock()
	defer tb.mu.Unlock()
	for k, v := range mp {
		if tb.update(k, v) {
			updated = true
		}
	}
	return updated
}
