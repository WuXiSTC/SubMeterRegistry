package Table

import "sync"

//一个线程安全的表
type Table struct {
	t  map[string]string
	mu sync.RWMutex
}

//返回表中全部内容
func (tb *Table) getAll() map[string]string {
	mp := make(map[string]string)
	tb.mu.RLock()
	defer tb.mu.Unlock()
	for k, v := range tb.t {
		mp[k] = v
	}
	return mp
}
