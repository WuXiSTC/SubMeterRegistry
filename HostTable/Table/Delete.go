package Table

func (tb *Table) delete(k string) bool {
	if _, ok := tb.t[k]; ok { //如果键值存在
		delete(tb.t, k) //就执行删除
		return true     //且返回true
	}
	return false //否则返回false
}

func (tb *Table) Delete(k string) bool {
	tb.mu.Lock()
	defer tb.mu.Unlock()
	return tb.delete(k)
}
