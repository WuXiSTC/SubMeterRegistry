package HostTable

var path = "/etc/hosts"

func Init(initPath, runPath string) error {
	err := FromFile(initPath)
	if err != nil {
		return err
	}
	path = runPath
	return nil
}
