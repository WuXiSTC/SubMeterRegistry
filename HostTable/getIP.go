package HostTable

import (
	"math/rand"
	"net"
)

func getIP(addr string) (net.IP, error) {
	ip := net.ParseIP(addr) //先判断是不是IP地址
	if ip != nil {          //如果就是一个IP地址
		return ip, nil //那就直接返回
	}
	ips, err := net.LookupIP(addr) //否则就视作域名，进行IP查询
	if err != nil {                //报错则返回错误
		return nil, err
	}
	return ips[rand.Intn(len(ips))], nil //那就随机选一个
}
